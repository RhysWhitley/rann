// ********************************************
// CLASS HEAD FILE: readDataPC.h
// This program is used to read training data of SOLO model
//
// Kuo-lin Hsu
// Oct. 06, 1997
// ********************************************
# ifndef _READDATAPC_H_
# define _READDATAPC_H_

# include <iostream.h>
# include <fstream.h>
# include <string.h>
# include <stdlib.h>
# include "MATRIX.h"

class readDataPC {

public:

    char    fileNameMinMax[120];
    char    fileNameWin[120], fileNameWout[120], fileNameFreq[120];
    char    typeOfSimu[120], fileNameData[120], fileNameErrMap[120];
    char    fileNameFinResult[120];
    char    fileNameEignValue[120], fileNameEignVector[120];
    int     nNode, nVars, ndaFactor, iniSeed, calThreshold;
    long    nda;
    float   **range;
    long    **FreqMap;

    // CONSTRUCTOR:
    readDataPC(){ };
    // READ BASIC INFORMATION:
    void read_init(char *nameInputFile);
    // READ COUNTER-PROPAGATION NETWORK WEIGHTS:
    // READ Win TRAINING DATA and READ Win, Wout, freqMap FOR SIMULATION DATA
    void readInHidOutWeights(float ***Win, float ***Wout, float ***eValue, float ****eVector);
    // READ RANGE OF INPUT VARIABLES:
    void readInputRange();
    // READ DATA FROM DATA FILES:
    void readTrainingData(float **data, float *z);
    // NORMALIZED DATA IN PREDEFINED RANGE:
    oid normalData(float **data);
    // DISTRUCTOR:
    ~readDataPC(){ };
};

# endif
