TARGET	= 	SOLO
SOURCE	= 	MATRIX.cc \
		cntProLSClass.cc LLSSearchClass.cc \
		matrixObj.cc SOLO.cc readDataPC.cc sofmFreqClass.cc \
		corrcoef.c eigsrt.c gaussj.c jacobi.c nrutil.c

TMPOBJ	=	$(SOURCE:%.cc=%.o)
OBJ	=	$(TMPOBJ:%.c=%.o)
CC 	= 	/usr/bin/gcc  -I.
CXX = 	/usr/bin/g++  -I.
LIB	=	-lm
RM	=	rm -f

$(TARGET)	: $(OBJ)
		  $(CXX) -o $(TARGET) $(OBJ) $(LIB)

clean 		: $(RM) $(TARGET) $(OBJ)

%.o		: %.cc 
		  $(CXX) -c $< -o $@
%.o		: %.c 
		  $(CC) -c $< -o $@
