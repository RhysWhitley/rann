// *************************************************
// CLASS: readDataPC.cc
// This program is used to read training data of SOLO model
//
// Kuo-lin Hsu
// Oct. 6, 1997
// ************************************************
# include "readDataPC.h"

// ***********************
void readDataPC:: read_init(char *nameInputFile) {
// ***********************

    // open the INF file
	ifstream in1(nameInputFile, ios:: in);
	if(!in1) {
	    cout << "Cannot Open File: " << nameInputFile << endl;
	    exit(1);
	}
    // read from INF file
	in1 >> nVars;			    // NUMBER OF INPUT VARIABLES
	in1 >> nNode;			    // NUMBER OF SOFM NODES (nNode x nNode)
	in1 >> ndaFactor;		    // NUMBER OF DATA USED IN PARAMETER ESTIMATION
	in1 >> fileNameWin;		    // FILE NAME OF INPUT-HIDDEN WEIGHTS
	in1 >> nda;			        // TOTAL NUMBER OF DATA
	in1 >> fileNameData;		// FILE NAME OF DATA
	in1 >> fileNameMinMax;		// FILE NAME OF NORMALIZING RANGE OF EACH VARIABLE
	in1 >> typeOfSimu;		    // TYPE OF SIMULATION: (training or simulation)
	in1 >> iniSeed;			    // INITIAL RANDOM SEED
	in1 >> calThreshold;		// CALCULATE HIDDEN-OUTPUT WEIGHTS WHEN DATA NUMBER > THRESHOLD
	in1 >> fileNameEignValue;	// FILE NAME OF EIGEN VALUE
	in1 >> fileNameEignVector;	// FILE NAME OF EIGEN VECTOR
	in1 >> fileNameWout;		// FILE NAME OF FINAL TRAINING OUTPUT WEIGHTS
	in1 >> fileNameFreq;		// FILE NAME OF FREQUENCY TABLE OF SOGM MAP
	in1 >> fileNameErrMap;		// FILE NAME OF RMSE OF EACH SOFM NODE
	in1 >> fileNameFinResult;	// FILE NAME OF RMSE OF EACH SOFM NODE
	in1.close();

	range   = M2D_alloc(2,nVars);
	FreqMap = IntM2Dd_alloc(nNode, nNode);

	cout << "PRINT OUT WHAT JUST READ:"                     << endl;
    cout << "nVars: "	            << nVars            	<< endl;
    cout << "nNode: "	            << nNode            	<< endl;
    cout << "ndaFactor: "	        << ndaFactor        	<< endl;
    cout << "fileNameWin: "	        << fileNameWin      	<< endl;
    cout << "nda: " 	            << nda              	<< endl;
    cout << "fileNameData: "        << fileNameData     	<< endl;
    cout << "fileNameMinMax: "      << fileNameMinMax       << endl;
    cout << "typeOfSimu: " 	        << typeOfSimu           << endl;
    cout << "iniSeed: " 	        << iniSeed              << endl;
    cout << "calThreshold: "        << calThreshold         << endl;
    cout << "fileNameEignValue: "   << fileNameEignValue    << endl;
    cout << "fileNameEignVector: "  << fileNameEignVector   << endl;
    cout << "fileNameWout: "        << fileNameWout         << endl;
    cout << "fileNameFreq: "        << fileNameFreq         << endl;
    cout << "fileNameErrMap: "      << fileNameErrMap       << endl;
    cout << "fileNameFinResult: "   << fileNameFinResult    << endl;
  }

// ****************************************************************
void readDataPC:: readInHidOutWeights(  float ***Win, float ***Wout,
                                        float ***eValue, float ****eVector
                                        ) {
// READ Win TRAINING DATA and READ Win, Wout, freqMap FOR SIMULATION DATA
// ****************************************************************

    int i, j, k, l, nV, cmp;
    char idchar[120];

    // open weight matrix from sofm
    ifstream in1(fileNameWin, ios:: in);
    if(!in1) {
        cout << "Cannot Open File: " <<  fileNameWin << endl;
        exit(1);
    }
    // weight matrix from sofm
	for (k=0; k<nVars; k++)
	    for(i=0; i<nNode; i++)
	        for(j=0; j<nNode; j++)
                in1 >> Win[i][j][k];
	in1.close();
	// ASSIGN Wout:
	strcpy(idchar,"training");
	cmp = strcmp(typeOfSimu,idchar);
    // if 0 then it's SOLO if 1 then seqSOLO, ... I think?
	if(cmp==0) {
        for(i=0; i<nNode; i++)
            for(j=0; j<nNode; j++) {
                for (k=0; k<=nVars; k++)
                    Wout[i][j][k] = -1.0;
                for (k=0; k<nVars; k++) {
                    eValue[i][j][k] = 1.0;
                    for (l=0; l<nVars; l++)
                        eVector[i][j][k][l] = 0.0;
                }
            }
	} else {
	    // READ HIDDEN-OUTPUT WEIGHTS:
	    ifstream in1(fileNameWout, ios:: in);
        if(!in1) {
            cout << "Cannot Open File: " <<  fileNameWout << endl;;
            exit(1);
        }
        for(i=0; i<nNode; i++)
            for(j=0; j<nNode; j++)
                for (k=0; k<=nVars; k++)
                    in1 >> Wout[i][j][k];
        in1.close();
	    // READ FREQUENCY MAP:
	    ifstream in2(fileNameFreq, ios:: in);
        if(!in2) {
            cout << "Cannot Open File: " <<  fileNameFreq << endl;
            exit(1);
        }
        for(i=0; i<nNode; i++)
            for(j=0; j<nNode; j++)
                in2 >> FreqMap[i][j];
        in2.close();
	    // READ EIGEN VALUES:
        ifstream in3(fileNameEignValue, ios:: in);
        if(!in3) {
            cout << "Cannot Open File: " <<  fileNameEignValue << endl;;
            exit(1);
        }
        for(i=0; i<nNode; i++)
            for(j=0; j<nNode; j++)
                for (k=0; k<nVars; k++)
                    in3 >> eValue[i][j][k];
        in3.close();
        // READ EIGEN VECTORS:
        ifstream in4(fileNameEignVector, ios:: in);
        if(!in4) {
            cout << "Cannot Open File: " <<  fileNameEignVector << endl;;
            exit(1);
        }
        for(i=0; i<nNode; i++)
            for(j=0; j<nNode; j++)
                for (k=0; k<nVars; k++)
                    for (l=0; l<nVars; l++)
                        in4 >> eVector[i][j][k][l];
        in4.close();
	}
}

// ***************************
void readDataPC:: readInputRange() {
// ***************************
    int i, j;

	ifstream in1(fileNameMinMax, ios:: in);
	if(!in1) {
	    cout << "Cannot Open File: " <<  fileNameMinMax << endl;
	    exit(1);
	}
	for(i=0; i<2; i++)
	    for(j=0; j<nVars; j++)
            in1 >> range[i][j];
  	in1.close();
}

// *******************************
void readDataPC::  readTrainingData(float **data, float *z) {
// *******************************
    int i,j;
    // data is the driver and Z is the independent variable to be fitted
    ifstream in1(fileNameData, ios:: in);
    if(!in1) {
        cout << "FILE OPEN ERROR: " << fileNameData << endl;
        exit(1);
    }
    for(i=0; i<nda; i++) {
        for(j=0; j<nVars; j++)
            in1 >> data[i][j];
        in1 >> z[i];
    }
    in1.close();
}

// **********************
void readDataPC::  normalData(float **data) {
// **********************
    long i, j;

    for(i=0; i<nda; i++)
        for(j=0; j<nVars; j++)
            data[i][j] = (data[i][j]-range[1][j]) / (range[0][j]-range[1][j]);
}


