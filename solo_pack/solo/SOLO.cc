// ***********************************************************************************
// SOLO main program:
//
//	Kuo-lin Hsu
//	2/27/98
// ***********************************************************************************

# include <Rcpp.h>
# include "readDataPC.h"
# include "sofmFreqClass.h"
# include "LLSSearchClass.h"
# include <iomanip.h>

RcppExport SEXP solo( SEXP R_iDATA, SEXP R_iSOFM, SEXP R_iINFO ) {

    char    fnameOut9[120];
    long    i, j, k, l, m;
    int     nVars, node, ndaFactor, cntFreq;
    float   **acmRR, **acmErr, **x, *zObs, *zOut;
    float   ***Wi, ***Wo, ***eValue, ****eVector;
    long    nda, ***indSofmDa, sum;
    float   rmse, error;
    // moved out of the calculation loops
    long    nd0, nd1, nd2;
    int     ix, jy, thresHold, nWn=-1;
    int     sTop=0;
    float   **dataNode, *zobsNode;

    // Declare new SEXP object wrappers
    Rcpp::NumericMatrix iDATA( R_iDATA );
    Rcpp::NumericMatrix iSOFM( R_iSOFM );
    Rcpp::List          iINFO( R_iINFO );

    readDataPC      WW; // << this can be replaced by passes from R
    sofmFreqClass   XX;
    LLSSearchClass  YY;

    // GET SOME IMPORTANT PARAMETERS:
    node        = iSOFM.nrow();
    nVars       = iDATA.ncol();
    nda         = iDATA.nrow();
    ndaFactor   = iINFO["ndaF"];
    cntFreq     = iINFO["calcT"];
    if( strcmp(iINFO["simtype"],"training")==0 )
        cntFreq = 1;

    // MEMORY ALLOCATION:
    Wi      =   M3D_alloc (node, node, nVars);
    Wo      =   M3D_alloc (node, node, nVars+1);
    eValue  =   M3D_alloc (node, node, nVars);
    eVector =   M4D_alloc (node, node, nVars, nVars);
    acmRR   =   M2D_alloc (node, node);
    acmErr  =   M2D_alloc (node, node);

    // x = imported drivers from R
    // Zobs = predictant from R
    x       =   M2D_alloc (nda, nVars);    // predictors
    zObs    =   V_alloc   (nda);           // response
    zOut    =   V_alloc   (nda);           // simulated

// Move this up to be allocated by a passed R list
    // READ INPUT-HIDDEN AND HIDDEN-OUTPUT WEIGHTS:
    WW.readInHidOutWeights(Wi, Wo, eValue, eVector);
    // READ INPUT RANGE:
    WW.readInputRange();
    // READ TRAINING DATA AND PUT THEM TO x and zobs:
    WW.readTrainingData(x, zObs);
    // NORMAILZED THE INPUT DATA:
    WW.normalData(x);
// <<<<<<<<<<<<<<<<<<<,,

// BELOW HERE SHOULDN'T NEED TO BE MODIFIED"
//
    // INITIAL CntProLSClass:
    XX.cnt_init(node, nVars, cntFreq, Wi, Wo, x[0], eValue, eVector);
    // INITIAL sofmFreqClass:
    XX.sofm_init(nda,x);
    // FIND FREQUENCY TABLE:
    XX.findSofmFreq();

    sum=0;
    for (i=0; i<node; i++) {
        for (j=0; j<node; j++) {
            acmRR[i][j]     = 0.0;
            acmErr[i][j]    = 0.0;
            fo3 << setw(8) << setprecision(1) << XX.freq[i][j] << " " ;
            cout << setw(8) << setprecision(6) << XX.freq[i][j] << " ";
            sum += XX.freq[i][j];
        }
        fo3 << endl;
        cout << endl;
    }
    fo3 << "total training data: " << sum << endl;

    // FIND DATA AT EACH SOFM NODE:
    XX.findHidOutputEachNode();
    // INITIAL YY CntProNet:
    YY.cnt_init(node, nVars, cntFreq, Wi, Wo, x[0], eValue, eVector);

    // TRAINING HIDDEN-OUTPUT WEIGHTS USING LINEAR LEAST SQUARE:
    for( i=0; i<node; i++ )
    	for( j=0; j<node; j++ ) {
        // GET DATA FROM SOFM NODE:
            nWn       = -1;
            sTop      = 0;
            nd0       = 0;
            thresHold = (nVars+1) * ndaFactor;
            while( nd0<=thresHold && sTop==0 ) {
                nd0 = 0;
                nWn += 1;
                for(    ix=i-nWn; ix<=i+nWn; ix++ )
                    for(jy=j-nWn; jy<=j+nWn; jy++ ) {
                        if( ix>=0 && ix<node && jy>=0 && jy<node )
                            nd0 += XX.freq[ix][jy];
                    }
                if( nWn>=node )
                    sTop = 1;
            }
            cout << endl;
            cout << "sTop= " << sTop << "  thresHold= " << thresHold << "  nd0= " << nd0 << endl;
            if( sTop==0 ) {
                dataNode = M2D_alloc(nd0,nVars);
                zobsNode = V_alloc(nd0);
                nd2 = -1;
                for(    ix=i-nWn; ix<=i+nWn; ix++)
                    for(jy=j-nWn; jy<=j+nWn; jy++)
                        if(ix >=0 && ix <node && jy >=0 && jy < node) {
                            if(XX.freq[ix][jy]>0) {
                                nd1 = nd2+1;
                                nd2 = nd1 + XX.freq[ix][jy]-1;
                                for (k=nd1; k<=nd2; k++) {
                                    m = XX.indSofmDa[ix][jy][k-nd1];
                                    for (l=0; l<nVars; l++)
                                        dataNode[k][l] = x[m][l];
                                    zobsNode[k] = zObs[m];
                                    acmRR[i][j] += zObs[m];
                                }
                            }
                    }
                cout << "nodeX= " << i << "  nodeY= " << j << "  nWn= " << nWn << "  nda= " << nd0 << endl;
                cout << "Wo BEFORE LS ESTIMATES: ";
                for (jy=0; jy<=nVars; jy++)
                    cout << Wo[i][j][jy] << "  ";
                cout << endl;
                cout << " *************************** " << endl;
                // INITIAL SEARCH:
                YY.lls_init(nVars, i, j, nd0, dataNode, zobsNode);
                // LINEAR LEAST SQUARE SEARCH:
                YY.linearLeastSquare();
                cout << "Wo AFTER LS ESTIMATES: ";
                for (jy=0; jy<=nVars; jy++)
                    cout << Wo[i][j][jy] << "  ";
                cout << endl;
                M2DFree(dataNode, nd0);
                free(zobsNode);
            }
        }
    rmse = 0;
    for(i=0; i<nda; i++) {
        YY.putInputData(x[i]);
        YY.produceOutputFromSingleInput(XX.freq, &zOut[i]);
        error = zObs[i] - zOut[i];
        rmse +=  pow( error, 2.0 );
        acmErr[YY.indexX][YY.indexY] += error;
        fo9 << setw(10) << setprecision(4) << zObs[i] << "   "  << zOut[i] << "  " << error << endl;
    }
    rmse = rmse / (float) (nda-1);
    rmse = pow (rmse, 0.5);
    fo3 << "rmse after training = " << rmse << endl;
    // GET FINAL Wi & Wo WEIGHTS:
    YY.passWinWout(Wi,Wo);
    // PRINT OUT INPUT-HIDDEN WEIGHTS AND FINAL HIDDEN-OUTPUT WEIGHTS:
    for(i=0; i<nVars; i++)
        for (j=0; j<node; j++) {
            for (k=0; k<node; k++)
                fo1 << setw(12) << setprecision(6) << Wi[j][k][i];
            fo1 << endl;
        }
    for (i=0; i<node; i++)
        for (j=0; j<node; j++) {
            for (k=0; k<=nVars; k++)
                fo2 << " " << setw(12) << setprecision(6)<< Wo[i][j][k];
            fo2 << endl;
        }
    for (i=0; i<node; i++) {
        for(j=0; j<node; j++) {
            for (k=0; k<nVars; k++)
                fo7 << setw(12) << setprecision(6) << eValue[i][j][k] << " ";
            for (k=0; k<nVars; k++)
                for (l=0; l<nVars; l++)
                    fo8 << setw(12) << setprecision(6) << eVector[i][j][k][l] << " ";
            fo5 << setw(12) << setprecision(3) << acmRR[i][j] << " ";
            fo6 << setw(12) << setprecision(3) << acmErr[i][j]<< " ";
            fo7 << endl;
            fo8 << endl;
        }
        fo5 << endl;
        fo6 << endl;
    }

    // MEMORY FREE
    M3DFree(Wi, node, node);
    M3DFree(Wo, node, node);
    M3DFree(eValue, node, node);
    M4DFree(eVector, node, node, nVars);
    M2DFree(acmRR, node);
    M2DFree(acmErr, node);
    M2DFree(x, nVars);
    free(zObs);
    free(zOut);

    return 0;
}   // END OF PROGRAM.

