/* ************************************************************
// Kohonen's Self-Organizing Neural Network
//
// Kuo-lin Hsu
// August 4, 1995
//
// Modifications to be called by R
// Rhys Whitley
// March 3, 2013
// ************************************************************/

// INCLUDE HEAD FILES
# include <R.h>
# include <Rdefines.h>
# include <Rinternals.h>
# include "sofm.h"

// DEFINE:
# define MAX(x,y)	x > y ? x : y
# define eta0	    0.5

SEXP sofm( SEXP R_inDATA, SEXP R_inLIST ) {

    // DEFINE VARIABLES
    // SEXP, vector and matrix pointers
    MATRIX3D	Wi, Wi_old;
    MATRIX      data, nodesum;
    SEXP        C_data, C_distN, C_wghtF, R_outTp, R_oLIST, R_oNAME;
    // define C variables:
    char    fin2[120], fo1[90], fo2[90], fo3[90], fo4[90];
    float   coef, fmin, tmp, WeightingF, Wdis, width;
    long    nvar, nda, node, Nc0, Nc, ncyc;
    long    i1, i2, i3, i, j, k, Iseed;
    int     lbX, lbY, ubX, ubY, indX, indY, mx1, my1, mxy, NWch, dlen;
    int     m=0, P=0, Nwcyc[50] =
                {   0, 1, 2, 3, 15, 50, 75, 100, 125, 150,
                    175, 200, 225, 250, 275, 300, 325, 350, 375, 400,
                    425, 450, 475, 500, 550, 600, 650, 700, 750, 800,
                    850, 900, 950, 1000, 1050, 1100, 1150, 1200, 1250, 1300,
                    1400, 1500, 1600, 1700, 1800, 2000, 2500, 3000, 3500, 3999 };

    // INITIALISE ALL VARIABLES THAT WILL HANDLE DATA BETWEEN R AND C
    // extract parameters from the passed R-type list
    nda     = INTEGER   ( GET_DIM( R_inDATA ) )[0];
    nvar    = INTEGER   ( GET_DIM( R_inDATA ) )[1];
    node    = INTEGER   ( VECTOR_ELT( R_inLIST, 0 ) )[0]; // 0 index grabs the value in the SEXP
    ncyc    = INTEGER   ( VECTOR_ELT( R_inLIST, 1 ) )[0];
    NWch    = INTEGER   ( VECTOR_ELT( R_inLIST, 2 ) )[0];
    width   = REAL      ( VECTOR_ELT( R_inLIST, 3 ) )[0];
    Iseed   = INTEGER   ( VECTOR_ELT( R_inLIST, 4 ) )[0];
    dlen    = (int)ncyc/NWch;
    // arrays protected on the stack - stops R garbage collection on these variables
    C_data  = PROTECT( coerceVector( R_inDATA, REALSXP ) );             P++;
    // construct new SEXP objects that will be returned to R
    R_oLIST = PROTECT( Rf_allocVector( VECSXP, 2 ) );                   P++;
    R_oNAME = PROTECT( Rf_allocVector( STRSXP, 2 ) );                   P++;
    C_distN = PROTECT( Rf_allocMatrix( REALSXP, dlen, 6 ) );            P++;
    C_wghtF = PROTECT( Rf_alloc3DArray( REALSXP, node, node, nvar ) );  P++;
    // count the number protect objects on the stack

    // assign memory for variables
    Wi		= M3D_alloc( node,   node,    nvar );
    Wi_old	= M3D_alloc( node,   node,    nvar );
    data	= M2D_alloc( nda,    nvar+1 );
    nodesum	= M2D_alloc( node,   node );

    // Copy in normalised data for training
    for( i=0; i<nda; i++ )
        for( j=0; j<nvar; j++ ) {
            data[i][j] = REAL(C_data)[j*nda+i];
        }

    // INITIALIZING WEIGHT MATRIX: Wi[i][j][k]
    srand(Iseed);
    for (k=0;k<nvar;k++)
        for (i=0;i<node;i++) {
    	    for (j=0;j<node;j++) {
                tmp = ((1.0*rand())/(1.0*RAND_MAX)) * width;
                Wi[i][j][k] = 0.5 + (tmp - width/2.0);
                Wi_old[i][j][k] = Wi[i][j][k];
            }
        }

    // LEARNING:
    //          1. SUMMATION TOTAL OF INPUTS AND WEIGHTS
    //          2. TRAINING POINTS CLOSING TO BEST FITED POINT (WITHIN Nc NEIGHBOR)
    //          3. LOWER BOUNDS: lbX,lbY; UPPER BOUNDS: ubX,ubY

    Nc0 = node / 2;

    // TRAINING LOOP:
    for (i1=0;i1<ncyc;i1++) {
	// SETUP TRAINING COEFFICIENT AND TRAINING NEIGHBOURS
        coef = eta0 * (1.0 - (1.0*i1)/(1.0*ncyc) );
        if(coef < 0.02)	coef = 0.02;
        Nc   = (int)( Nc0 * (1.0 - (1.0*i1)/(1.0*ncyc)) );

    // DATA TRAINING LOOP
        for (i2=0;i2<nda;i2++) {

	    // FIND THE DISTANCE BETWEEN INPUTS AND WEIGHT VECTOR
	    // FIND THE POINT WITH MINIMUM DISTANCE TO THE WEIGHT VECTOR
	    // 		AND IT'S INDEX (indX and indY)
            fmin=1.0e10;
            for (i=0;i<node;i++) {
                for (j=0;j<node;j++) {
                    nodesum[i][j]=0.0;
                    for (k=0;k<nvar;k++) {
			// FIND THE DISTANCE BETWEEN INPUTS AND WEIGHT VECTOR
                        tmp = data[i2][k]-Wi[i][j][k];
                        nodesum[i][j]+=tmp*tmp;
                    }
                    nodesum[i][j] = pow(nodesum[i][j],0.5);
                    // FIND THE MINIMUM NODE AND ITS INDEX:
                    if(nodesum[i][j] < fmin) {
                        fmin = nodesum[i][j];
                        indX = i;
                        indY = j;
                    }
                }
            }

	    // FIND THE TRAINING UPPER BOUNDS AND LOWER BOUNDS
            lbX=indX-Nc;    ubX=indX+Nc;
            lbY=indY-Nc;    ubY=indY+Nc;
            if(lbX<0) lbX=0;
            if(ubX>node-1) ubX=node-1;
            if(lbY<0) lbY=0;
            if(ubY>node-1) ubY=node-1;

        // TRAINING INPUT WEIGHTS AND OUTPUT WEIGHTS WITHIN
        // 	NEIGHBORHOOD Nc AROUND THE BEST POINT
            for (i=lbX;i<ubX+1;i++)
                for (j=lbY;j<ubY+1;j++) {
		    // SETUP WEIGHTING FACTOR FOR POINTS AROUND THE BEST POINT
                    mx1 = abs(i-indX);
                    my1 = abs(j-indY);
                    mxy = MAX(mx1,my1);
                    WeightingF = 1.0 / (mxy + 1.0);
                    // WeightingF = 1.0;

		    // INPUTS & INPUT WEIGHTS
                    for (k=0;k<nvar;k++)
                        Wi[i][j][k] = Wi[i][j][k] + WeightingF*coef*(data[i2][k]-Wi[i][j][k]);
                }

        } // END OF PATTERN LOOP

	// PRINT THE DISTANCE BETWEEN TWO CONNECTION WEIGHTS WITH NWch ITERATION:
        if((i1%NWch)==0) {
            tmp = 0;
            for (i=0; i<node; i++)
                for (j=0; j<node; j++)
                    for (k=0; k<nvar; k++) {
                        tmp += pow((Wi[i][j][k]-Wi_old[i][j][k]),2.0);
                        Wi_old[i][j][k] = Wi[i][j][k];
                    }
            Wdis = pow(tmp, 0.5);
            REAL(C_distN)[m+dlen*0] = (double)i1;
            REAL(C_distN)[m+dlen*1] = (double)Nc0;
            REAL(C_distN)[m+dlen*2] = (double)Nc;
            REAL(C_distN)[m+dlen*3] = (double)eta0;
            REAL(C_distN)[m+dlen*4] = (double)coef;
            REAL(C_distN)[m+dlen*5] = (double)Wdis;
            m++;
            printf( "  %ld\t  %ld  %ld  %f  %f  %f\n", i1, Nc0, Nc, eta0, coef, Wdis );
        }

    } // END OF ITERATION LOOP

    // CREATE OUTPUT LIST TO BE RETURNED TO R
    // copy final weight vector to R SEXP vector
    for(k=0; k<nvar; k++)
        for (i=0; i<node; i++)
            for (j=0; j<node; j++)
                REAL(C_wghtF)[k*node*node+j*node+i] = Wi[i][j][k];    // << this iterates each array's row, then moves to next array
    // place arrays into an R list to be returned
    SET_VECTOR_ELT( R_oLIST, 0, C_wghtF );
    SET_VECTOR_ELT( R_oLIST, 1, C_distN );

    // FREE MEMORY
    M3DFree(Wi,         node,   node);
    M3DFree(Wi_old,     node,   node);
    M2DFree(data,       nda);
    M2DFree(nodesum,    node);
    UNPROTECT(P);   // allow R to garbage collect

    return(R_oLIST);
}
