// **********************************************
// SEQUENTAIL TRAINING OF SOLO MODEL
// 
// Kuo-lin Hsu
// 10/12/97
// **********************************************
# include "readDataPC.h"
# include "cntProLSClass.h"
# include <iostream.h>
# include <fstream.h>
# include <iomanip.h>

# define  fnameIn1	"seqSOLO.inf"
# define  fnameIn2	"learningRate.inf"
# define  fnameRMSE	"trainingRMSE.out"
# define  fnameSeq0	"seqOut0.out"	// output from preseted parameters
# define  fnameSeq1	"seqOut1.out"	// output from adaptive training (non-fixed parameters)
# define  fnameSeq2	"seqOut2.out"	// output from fixed parameters after adaptive training
# define  fnameSeq3	"seqHidOutW.out"
# define  fnameSeq4	"seqFreqMap.out"

main(){
float **x, *zobs, *zout;
float ***Wi, ***Wo;
float ***eValue, ****eVector;
float *rmseSeq, *rmseBat;
float learningRate;
float tmp, err;
float noObsData;
int   i, j, k, l, cnt;
int node, nVars;
int **freqMap;
long nda, iteration;
readDataPC	XX;
cntProLSClass	YY;

    // READ INFORMAITON FROM readData Class
    XX.read_init(fnameIn1);

    // ASSIGN LEARNING RATE:
    ifstream in1(fnameIn2, ios:: in);
    if(!in1) {
	cout << "Can't Open File: " << fnameIn2 << endl;
	exit(1);
    }
    in1 >> learningRate;
    in1 >> iteration;
    cout << "Learning Rate = " << learningRate << endl;
    cout << "No. of Iteration = " << iteration << endl;

    // ASSIGN SEVERAL PARAMETERS:
    node  	= XX.nNode;
    nVars  	= XX.nVars;
    nda		= XX.nda;
    noObsData	= XX.noObsData;

    // MEMORY ALLOCATION FOR CONNECTION WEIGHTS:
    Wi  	=       M3D_alloc (node, node, nVars);
    Wo  	=       M3D_alloc (node, node, nVars+1);
    eValue      =       M3D_alloc (node, node, nVars+1);
    eVector     =       M4D_alloc (node, node, nVars+1, nVars+1);
    x		=	M2D_alloc (nda,   nVars);
    zobs	=	V_alloc	(nda);
    zout	=	V_alloc	(nda);
    rmseSeq	=	V_alloc (iteration);
    rmseBat	=	V_alloc (iteration);
    freqMap	= 	IntM2D_alloc (node, node);

    // READ INPUT-HIDDEN AND HIDDEN-OUTPUT WEIGHTS:
    XX.readInHidOutWeights(Wi, Wo, eValue, eVector);

    // READ INPUT RANGE:
    XX.readInputRange();

    // READ TIME TRAINING DATA:
    XX.readTrainingData(x, zobs);

    // NORMALIZED DATA TRAINING DATA:
    XX.normalData(x);

    // INITIAL cntProBasicClass:
    YY.cnt_init(node, nVars, XX.calThreshold, Wi, Wo, x[0], eValue, eVector, noObsData);

    ofstream o1(fnameRMSE, ios:: out);
    ofstream o2a(fnameSeq0, ios:: out);
    ofstream o2(fnameSeq1, ios:: out);
    ofstream o3(fnameSeq2, ios:: out);
    ofstream o4(fnameSeq3, ios:: out);
    ofstream o5(fnameSeq4, ios:: out);
    o2a.setf(ios:: fixed);	o2a.precision(4);
    o2.setf(ios:: fixed);	o2.precision(4);
    o3.setf(ios:: fixed);	o3.precision(4);
    o4.setf(ios:: fixed);	o4.precision(4);

    for (i=0; i<node; i++) for (j=0; j< node; j++) freqMap[i][j] = 0;

    // ESTIMATE SIMULATED OUTPUT: (FIX WEIGHTS)
    for(i=0; i<nda; i++) {
        YY.putInputData(x[i]);
        YY.produceOutputFromSingleInput(XX.FreqMap,&zout[i]);
	//if(zout[i] < 0.0) zout[i] = 0.0; 
	err = zout[i] - zobs[i];
	if(zobs[i] == noObsData) err = 0.0;
	o2a << setw(10) << zobs[i] << "  " << setw(10) << zout[i] << "  " << setw(10) << err << endl;
	freqMap[YY.indexX][YY.indexY]++;
// cout << i << " " << YY.indexX << " " << YY.indexY;
// for (j=0; j<nVars; j++) cout << " " << x[i][j];
// cout << " " << zobs[i] << " " << zout[i] << endl;
    }

    // TRAINING HIDDEN-OUTPUT WEIGHTS FROM GIVEN TARGET DATA:
    if(learningRate > 0.0 && iteration >1) {
        for (k=0; k<iteration ; k++) {
	    cout << "Training iteration No.: " << k ;
	    tmp=0.0;  cnt=0;
    	    for (i=0; i<nda; i++) {
        	YY.putInputData(x[i]);
        	YY.produceOutputFromSingleInput(XX.FreqMap,&zout[i]);
        	//if(zout[i]<0.0) zout[i] = 0.0;
		if(zobs[i] != noObsData) {
			err = zout[i] - zobs[i]; 
			tmp += err * err;
			YY.realTimeTraining(learningRate, XX.FreqMap, x[i], &zobs[i]);
			cnt++;
		}
	    }
	    // RMSE OF SEQUENTIAL TRAINING WITH VARYING AND FIXING WEIGHTS:
	    if(cnt>2) {
	        rmseSeq[k] = tmp / (cnt-1.0);
	        rmseSeq[k] = pow(rmseSeq[k], 0.5);
	        YY.produceRMSE(XX.FreqMap, nda, x, zobs, &rmseBat[k]);
	        o1 << rmseSeq[k] << "  " << rmseBat[k] << endl;
	        cout << "  " << "rmseSeq= " << rmseSeq[k] << "   rmseBat= " << rmseBat[k] << endl;
	    }
        }
    }

    // ESTIMATE SIMULATED OUTPUT: (FIX WEIGHTS)
    for(i=0; i<nda; i++) {
        YY.putInputData(x[i]);
        YY.produceOutputFromSingleInput(XX.FreqMap,&zout[i]);
        //if(zout[i]<0.0) zout[i] = 0.0;
	err = zout[i] - zobs[i];
	if(zobs[i] == noObsData) err = 0.0;
	o2 << setw(10) << zobs[i] << "  " << setw(10) << zout[i] << "  " << setw(10) << err << endl;
    }

    // ESTIMATE SIMULATED OUTPUT: (ADAPTIVE WEIGHTS)
    tmp=0.0;  cnt=0;
    for (i=0; i<nda; i++) {
        YY.putInputData(x[i]);
        YY.produceOutputFromSingleInput(XX.FreqMap,&zout[i]);
        //if(zout[i]<0.0) zout[i] = 0.0;
	if(zobs[i] != noObsData) {
            err = zout[i] - zobs[i];
	    tmp += err * err;
            YY.realTimeTraining(learningRate, XX.FreqMap, x[i], &zobs[i]);
	    cnt++;
	}
	else { err=0.0; }
        o3 << setw(10) << zobs[i] << "  " << setw(10) << zout[i] << "  " << setw(10) << err << endl;
    }
    if(cnt >=2) {
	float rmseS, rmseB;
        rmseS = tmp / (float) (cnt-1.0);
        rmseS = pow(rmseS, 0.5);
        YY.produceRMSE(XX.FreqMap, nda, x, zobs, &rmseB);
        o1 << rmseS << "  " << rmseB << endl;
        cout << "  " << "rmseSeq= " << rmseS << "   rmseBat= " << rmseB << endl;
    }
    // OUTPUT FINAL UPDATED TRAINING WEIGHTS:
    for(i=0; i<node; i++)
        for(j=0; j<node; j++) {
            for (k=0; k<=nVars; k++)
                            o4 << " " << setw(10) <<  YY.wOut[i][j][k];
	    o4 << endl;
	}

    // OUTPUT FINAL FREQUENCY MAP:
    for (i=0; i<node; i++) {
	for (j=0; j<node; j++)
	    o5 << "  " << freqMap[i][j];
	o5 << endl;
    }

    o1.close();
    o2a.close();
    o2.close();
    o3.close();
    o4.close();
    o5.close();
}

